f = open('text.txt','r')
s = f.read()
f.close()

if s[0:3] == 'abc':
    s = s.replace('abc', 'www')
else:
    s = s + 'zzz'

f = open('text.txt', 'a')
f.write('\n --New data--\n')
f.write(s)
f.close()